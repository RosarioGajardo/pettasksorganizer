﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetTasks
{
    public interface IAnimal
    {
        string Name { get; set; }
        string FoodType { get; set; }
        void FeedMeNotification();
        void CleanMeNotification();
        bool FeedDone { get; set; }
        bool CleanDone { get; set; }
        //bool TreatDone();
       // List<DateTime> TimeForTasks();
    }

    public class Dog : IAnimal
    {
        public Dog(string name)
        {
            Name = name;
        }
        public string Name { get { return Name; } set { Name = "Dog"; }}
        public string FoodType { get { return FoodType; } set { FoodType = "Kibble/Dry food"; }}
        public bool FeedDone { get { return FeedDone; } set { FeedDone = false; } }
        public bool CleanDone { get { return CleanDone; } set { CleanDone = false; } }

        public void FeedMeNotification()
        {
            Console.WriteLine("Feed Me");
        }
        public void CleanMeNotification()
        {
            Console.WriteLine("Clean Me");
        }
    }

    public class Cat : IAnimal
    {
        public Cat(string name)
        {
            Name = name;
        }
        public string Name { get { return Name; } set { Name = "Cat"; } }
        public string FoodType { get { return FoodType; } set { FoodType = "Kibble/Dry food"; } }
        public bool FeedDone { get { return FeedDone; } set { FeedDone = false; } }
        public bool CleanDone { get { return CleanDone; } set { CleanDone = false; } }

        public void FeedMeNotification()
        {
            Console.WriteLine("Feed Me");
        }
        public void CleanMeNotification()
        {
            Console.WriteLine("Clean Me");
        }
        
    }

    public class Fish : IAnimal
    {
        public Fish(string name, int aquariumId)
        {
            Name = name;
            AquariumId = aquariumId;
        }
        public string Name { get { return Name; } set { Name = "Fish"; } }
        public string FoodType { get { return FoodType; } set { FoodType = "Kibble/Dry food"; } }
        public bool FeedDone { get { return FeedDone; } set { FeedDone = false; } }
        public bool CleanDone { get { return CleanDone; } set { CleanDone = false; } }
        public int AquariumId;

        public void FeedMeNotification()
        {
            Console.WriteLine("Feed Me");
        }
        public void CleanMeNotification()
        {
            Console.WriteLine("Clean Me");
        }
    }

    public abstract class Rodent 
    {
        public abstract string LeisureType { get; set; }
        
    }

    public class Hamster : Rodent, IAnimal
    {
        public Hamster(string name)
        {
            Name = name;
        }
        public override string LeisureType  {get { return LeisureType; } set { LeisureType = "Wheel"; }}
        public string Name { get { return Name; } set { Name = "Hamster"; } }
        public string FoodType { get { return FoodType; } set { FoodType = "muesli, pellets"; } }
        public bool FeedDone { get { return FeedDone; } set { FeedDone = false; } }
        public bool CleanDone { get { return CleanDone; } set { CleanDone = false; } }

        public void FeedMeNotification()
        {
            Console.WriteLine("Feed Me");
        }
        public void CleanMeNotification()
        {
            Console.WriteLine("Clean Me");
        }
        public override string ToString()
        {
            return String.Format("Hamster's food type:{0}, for leisure they use:{1}", FoodType, LeisureType);
        }
    }

    public class GuineaPig : Rodent, IAnimal
    {
        public GuineaPig(string name)
        {
            Name = name;
        }
        public override string LeisureType { get { return LeisureType; } set { LeisureType = "Toys"; } }
        public string Name { get { return Name; } set { Name = "Guinea Pig"; } }
        public string FoodType { get { return FoodType; } set { FoodType = "Veggies, fruit, pellets"; } }
        public bool FeedDone { get { return FeedDone; } set { FeedDone = false; } }
        public bool CleanDone { get { return CleanDone; } set { CleanDone = false; } }

        public void FeedMeNotification()
        {
            Console.WriteLine("Feed Me");
        }
        public void CleanMeNotification()
        {
            Console.WriteLine("Clean Me");
        }

        
    }
    class Program
    {
        static void Main(string[] args)
        {
            Hamster stuart = new Hamster("stuart");
            stuart.ToString();
        }
    }
}
