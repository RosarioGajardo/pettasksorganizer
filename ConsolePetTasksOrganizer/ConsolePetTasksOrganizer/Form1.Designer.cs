﻿namespace ConsolePetTasksOrganizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.lblnameEmployee = new System.Windows.Forms.Label();
            this.txtBoxNameEmployee = new System.Windows.Forms.TextBox();
            this.txtboxLastName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxAnimalName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.btnAddAnimal = new System.Windows.Forms.Button();
            this.cmbBoxAnimalClasses = new System.Windows.Forms.ComboBox();
            this.btnRemoveEmployee = new System.Windows.Forms.Button();
            this.btnRemoveAnimal = new System.Windows.Forms.Button();
            this.btnTasksCompleted = new System.Windows.Forms.Button();
            this.lblTasksCompletedText = new System.Windows.Forms.Label();
            this.pnlTasksDone = new System.Windows.Forms.FlowLayoutPanel();
            this.btnTasksDoneByClass = new System.Windows.Forms.Button();
            this.cmbBoxClassTasksDone = new System.Windows.Forms.ComboBox();
            this.lblTasksDoneCounter = new System.Windows.Forms.Label();
            this.lblEmployeesCounter = new System.Windows.Forms.Label();
            this.lblEmployeesCounterText = new System.Windows.Forms.Label();
            this.lblAnimalsCounter = new System.Windows.Forms.Label();
            this.lblAnimalsCounterText = new System.Windows.Forms.Label();
            this.lblEmployeeAddedMessage = new System.Windows.Forms.Label();
            this.lblAnimalAddedMessage = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblErrorFeed = new System.Windows.Forms.Label();
            this.CheckBoxPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.txtBoxPetName = new System.Windows.Forms.TextBox();
            this.cmbBoxTasksOptions = new System.Windows.Forms.ComboBox();
            this.txtBoxHours = new System.Windows.Forms.TextBox();
            this.txtBoxMinutes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbBoxAMPM = new System.Windows.Forms.ComboBox();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.pnlMarkTaskComplete = new System.Windows.Forms.FlowLayoutPanel();
            this.lblUncompletedPanel = new System.Windows.Forms.Label();
            this.lblMarkCompletePanel = new System.Windows.Forms.Label();
            this.lblCompletedTasksPanel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTotalTasksCounter = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblAddTaskMessage = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Location = new System.Drawing.Point(28, 105);
            this.btnAddEmployee.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(85, 48);
            this.btnAddEmployee.TabIndex = 1;
            this.btnAddEmployee.Text = "Add employee";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // lblnameEmployee
            // 
            this.lblnameEmployee.AutoSize = true;
            this.lblnameEmployee.Location = new System.Drawing.Point(29, 29);
            this.lblnameEmployee.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblnameEmployee.Name = "lblnameEmployee";
            this.lblnameEmployee.Size = new System.Drawing.Size(45, 17);
            this.lblnameEmployee.TabIndex = 2;
            this.lblnameEmployee.Text = "Name";
            this.lblnameEmployee.Click += new System.EventHandler(this.lblnameEmployee_Click);
            // 
            // txtBoxNameEmployee
            // 
            this.txtBoxNameEmployee.Location = new System.Drawing.Point(110, 29);
            this.txtBoxNameEmployee.Margin = new System.Windows.Forms.Padding(4);
            this.txtBoxNameEmployee.Name = "txtBoxNameEmployee";
            this.txtBoxNameEmployee.Size = new System.Drawing.Size(132, 22);
            this.txtBoxNameEmployee.TabIndex = 3;
            // 
            // txtboxLastName
            // 
            this.txtboxLastName.Location = new System.Drawing.Point(110, 65);
            this.txtboxLastName.Margin = new System.Windows.Forms.Padding(4);
            this.txtboxLastName.Name = "txtboxLastName";
            this.txtboxLastName.Size = new System.Drawing.Size(132, 22);
            this.txtboxLastName.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 68);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Last Name";
            // 
            // txtBoxAnimalName
            // 
            this.txtBoxAnimalName.Location = new System.Drawing.Point(81, 88);
            this.txtBoxAnimalName.Margin = new System.Windows.Forms.Padding(4);
            this.txtBoxAnimalName.Name = "txtBoxAnimalName";
            this.txtBoxAnimalName.Size = new System.Drawing.Size(160, 22);
            this.txtBoxAnimalName.TabIndex = 10;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(28, 92);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 9;
            this.lblName.Text = "Name";
            // 
            // btnAddAnimal
            // 
            this.btnAddAnimal.Location = new System.Drawing.Point(31, 121);
            this.btnAddAnimal.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddAnimal.Name = "btnAddAnimal";
            this.btnAddAnimal.Size = new System.Drawing.Size(81, 48);
            this.btnAddAnimal.TabIndex = 8;
            this.btnAddAnimal.Text = "Add Animal";
            this.btnAddAnimal.UseVisualStyleBackColor = true;
            this.btnAddAnimal.Click += new System.EventHandler(this.btnAddAnimal_Click);
            // 
            // cmbBoxAnimalClasses
            // 
            this.cmbBoxAnimalClasses.FormattingEnabled = true;
            this.cmbBoxAnimalClasses.Items.AddRange(new object[] {
            "Dog",
            "Cat",
            "Hamster",
            "Guinea Pig"});
            this.cmbBoxAnimalClasses.Location = new System.Drawing.Point(31, 55);
            this.cmbBoxAnimalClasses.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBoxAnimalClasses.Name = "cmbBoxAnimalClasses";
            this.cmbBoxAnimalClasses.Size = new System.Drawing.Size(210, 24);
            this.cmbBoxAnimalClasses.TabIndex = 11;
            this.cmbBoxAnimalClasses.Text = "Type";
            // 
            // btnRemoveEmployee
            // 
            this.btnRemoveEmployee.Location = new System.Drawing.Point(142, 105);
            this.btnRemoveEmployee.Margin = new System.Windows.Forms.Padding(4);
            this.btnRemoveEmployee.Name = "btnRemoveEmployee";
            this.btnRemoveEmployee.Size = new System.Drawing.Size(100, 48);
            this.btnRemoveEmployee.TabIndex = 12;
            this.btnRemoveEmployee.Text = "Remove Employee";
            this.btnRemoveEmployee.UseVisualStyleBackColor = true;
            this.btnRemoveEmployee.Click += new System.EventHandler(this.btnRemoveEmployee_Click);
            // 
            // btnRemoveAnimal
            // 
            this.btnRemoveAnimal.Location = new System.Drawing.Point(141, 121);
            this.btnRemoveAnimal.Margin = new System.Windows.Forms.Padding(4);
            this.btnRemoveAnimal.Name = "btnRemoveAnimal";
            this.btnRemoveAnimal.Size = new System.Drawing.Size(100, 48);
            this.btnRemoveAnimal.TabIndex = 13;
            this.btnRemoveAnimal.Text = "Remove Animal";
            this.btnRemoveAnimal.UseVisualStyleBackColor = true;
            this.btnRemoveAnimal.Click += new System.EventHandler(this.btnRemoveAnimal_Click);
            // 
            // btnTasksCompleted
            // 
            this.btnTasksCompleted.Location = new System.Drawing.Point(28, 165);
            this.btnTasksCompleted.Margin = new System.Windows.Forms.Padding(4);
            this.btnTasksCompleted.Name = "btnTasksCompleted";
            this.btnTasksCompleted.Size = new System.Drawing.Size(132, 96);
            this.btnTasksCompleted.TabIndex = 15;
            this.btnTasksCompleted.Text = "Tasks Completed";
            this.btnTasksCompleted.UseVisualStyleBackColor = true;
            this.btnTasksCompleted.Click += new System.EventHandler(this.btnTasksCompleted_Click);
            // 
            // lblTasksCompletedText
            // 
            this.lblTasksCompletedText.AutoSize = true;
            this.lblTasksCompletedText.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTasksCompletedText.Location = new System.Drawing.Point(25, 29);
            this.lblTasksCompletedText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTasksCompletedText.Name = "lblTasksCompletedText";
            this.lblTasksCompletedText.Size = new System.Drawing.Size(135, 17);
            this.lblTasksCompletedText.TabIndex = 16;
            this.lblTasksCompletedText.Text = "Tasks completed:";
            // 
            // pnlTasksDone
            // 
            this.pnlTasksDone.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pnlTasksDone.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlTasksDone.Location = new System.Drawing.Point(1145, 111);
            this.pnlTasksDone.Margin = new System.Windows.Forms.Padding(4);
            this.pnlTasksDone.Name = "pnlTasksDone";
            this.pnlTasksDone.Size = new System.Drawing.Size(356, 662);
            this.pnlTasksDone.TabIndex = 26;
            // 
            // btnTasksDoneByClass
            // 
            this.btnTasksDoneByClass.Location = new System.Drawing.Point(179, 198);
            this.btnTasksDoneByClass.Margin = new System.Windows.Forms.Padding(4);
            this.btnTasksDoneByClass.Name = "btnTasksDoneByClass";
            this.btnTasksDoneByClass.Size = new System.Drawing.Size(131, 64);
            this.btnTasksDoneByClass.TabIndex = 27;
            this.btnTasksDoneByClass.Text = "Tasks Completed by Animal Type";
            this.btnTasksDoneByClass.UseVisualStyleBackColor = true;
            this.btnTasksDoneByClass.Click += new System.EventHandler(this.btnTasksDoneByClass_Click);
            // 
            // cmbBoxClassTasksDone
            // 
            this.cmbBoxClassTasksDone.FormattingEnabled = true;
            this.cmbBoxClassTasksDone.Items.AddRange(new object[] {
            "Dog",
            "Cat",
            "Hamster",
            "GuineaPig"});
            this.cmbBoxClassTasksDone.Location = new System.Drawing.Point(179, 165);
            this.cmbBoxClassTasksDone.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBoxClassTasksDone.Name = "cmbBoxClassTasksDone";
            this.cmbBoxClassTasksDone.Size = new System.Drawing.Size(132, 24);
            this.cmbBoxClassTasksDone.TabIndex = 28;
            // 
            // lblTasksDoneCounter
            // 
            this.lblTasksDoneCounter.AutoSize = true;
            this.lblTasksDoneCounter.Location = new System.Drawing.Point(194, 29);
            this.lblTasksDoneCounter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTasksDoneCounter.Name = "lblTasksDoneCounter";
            this.lblTasksDoneCounter.Size = new System.Drawing.Size(16, 17);
            this.lblTasksDoneCounter.TabIndex = 29;
            this.lblTasksDoneCounter.Text = "0";
            // 
            // lblEmployeesCounter
            // 
            this.lblEmployeesCounter.AutoSize = true;
            this.lblEmployeesCounter.Location = new System.Drawing.Point(194, 66);
            this.lblEmployeesCounter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmployeesCounter.Name = "lblEmployeesCounter";
            this.lblEmployeesCounter.Size = new System.Drawing.Size(16, 17);
            this.lblEmployeesCounter.TabIndex = 31;
            this.lblEmployeesCounter.Text = "0";
            // 
            // lblEmployeesCounterText
            // 
            this.lblEmployeesCounterText.AutoSize = true;
            this.lblEmployeesCounterText.Location = new System.Drawing.Point(25, 66);
            this.lblEmployeesCounterText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmployeesCounterText.Name = "lblEmployeesCounterText";
            this.lblEmployeesCounterText.Size = new System.Drawing.Size(81, 17);
            this.lblEmployeesCounterText.TabIndex = 30;
            this.lblEmployeesCounterText.Text = "Employees:";
            // 
            // lblAnimalsCounter
            // 
            this.lblAnimalsCounter.AutoSize = true;
            this.lblAnimalsCounter.Location = new System.Drawing.Point(194, 109);
            this.lblAnimalsCounter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAnimalsCounter.Name = "lblAnimalsCounter";
            this.lblAnimalsCounter.Size = new System.Drawing.Size(16, 17);
            this.lblAnimalsCounter.TabIndex = 33;
            this.lblAnimalsCounter.Text = "0";
            // 
            // lblAnimalsCounterText
            // 
            this.lblAnimalsCounterText.AutoSize = true;
            this.lblAnimalsCounterText.Location = new System.Drawing.Point(25, 109);
            this.lblAnimalsCounterText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAnimalsCounterText.Name = "lblAnimalsCounterText";
            this.lblAnimalsCounterText.Size = new System.Drawing.Size(61, 17);
            this.lblAnimalsCounterText.TabIndex = 32;
            this.lblAnimalsCounterText.Text = "Animals:";
            // 
            // lblEmployeeAddedMessage
            // 
            this.lblEmployeeAddedMessage.AutoSize = true;
            this.lblEmployeeAddedMessage.BackColor = System.Drawing.SystemColors.Control;
            this.lblEmployeeAddedMessage.Location = new System.Drawing.Point(38, 171);
            this.lblEmployeeAddedMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmployeeAddedMessage.Name = "lblEmployeeAddedMessage";
            this.lblEmployeeAddedMessage.Size = new System.Drawing.Size(0, 17);
            this.lblEmployeeAddedMessage.TabIndex = 34;
            // 
            // lblAnimalAddedMessage
            // 
            this.lblAnimalAddedMessage.AutoSize = true;
            this.lblAnimalAddedMessage.Location = new System.Drawing.Point(38, 182);
            this.lblAnimalAddedMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAnimalAddedMessage.Name = "lblAnimalAddedMessage";
            this.lblAnimalAddedMessage.Size = new System.Drawing.Size(0, 17);
            this.lblAnimalAddedMessage.TabIndex = 35;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(786, 171);
            this.lblError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 17);
            this.lblError.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(465, 796);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 37;
            // 
            // lblErrorFeed
            // 
            this.lblErrorFeed.AutoSize = true;
            this.lblErrorFeed.Location = new System.Drawing.Point(796, 322);
            this.lblErrorFeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrorFeed.Name = "lblErrorFeed";
            this.lblErrorFeed.Size = new System.Drawing.Size(0, 17);
            this.lblErrorFeed.TabIndex = 38;
            // 
            // CheckBoxPanel
            // 
            this.CheckBoxPanel.AutoScroll = true;
            this.CheckBoxPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.CheckBoxPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.CheckBoxPanel.Location = new System.Drawing.Point(345, 111);
            this.CheckBoxPanel.Margin = new System.Windows.Forms.Padding(2);
            this.CheckBoxPanel.Name = "CheckBoxPanel";
            this.CheckBoxPanel.Size = new System.Drawing.Size(361, 662);
            this.CheckBoxPanel.TabIndex = 40;
            this.CheckBoxPanel.WrapContents = false;
            // 
            // txtBoxPetName
            // 
            this.txtBoxPetName.Location = new System.Drawing.Point(32, 71);
            this.txtBoxPetName.Margin = new System.Windows.Forms.Padding(4);
            this.txtBoxPetName.Name = "txtBoxPetName";
            this.txtBoxPetName.Size = new System.Drawing.Size(210, 22);
            this.txtBoxPetName.TabIndex = 42;
            this.txtBoxPetName.Text = "Pet Name";
            // 
            // cmbBoxTasksOptions
            // 
            this.cmbBoxTasksOptions.FormattingEnabled = true;
            this.cmbBoxTasksOptions.Items.AddRange(new object[] {
            "Clean",
            "Feed",
            "Walk"});
            this.cmbBoxTasksOptions.Location = new System.Drawing.Point(32, 112);
            this.cmbBoxTasksOptions.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBoxTasksOptions.Name = "cmbBoxTasksOptions";
            this.cmbBoxTasksOptions.Size = new System.Drawing.Size(210, 24);
            this.cmbBoxTasksOptions.TabIndex = 43;
            // 
            // txtBoxHours
            // 
            this.txtBoxHours.Location = new System.Drawing.Point(32, 156);
            this.txtBoxHours.Margin = new System.Windows.Forms.Padding(4);
            this.txtBoxHours.Name = "txtBoxHours";
            this.txtBoxHours.Size = new System.Drawing.Size(26, 22);
            this.txtBoxHours.TabIndex = 44;
            this.txtBoxHours.Text = "0";
            // 
            // txtBoxMinutes
            // 
            this.txtBoxMinutes.Location = new System.Drawing.Point(82, 156);
            this.txtBoxMinutes.Margin = new System.Windows.Forms.Padding(4);
            this.txtBoxMinutes.Name = "txtBoxMinutes";
            this.txtBoxMinutes.Size = new System.Drawing.Size(26, 22);
            this.txtBoxMinutes.TabIndex = 45;
            this.txtBoxMinutes.Text = "00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 159);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 17);
            this.label3.TabIndex = 46;
            this.label3.Text = ":";
            // 
            // cmbBoxAMPM
            // 
            this.cmbBoxAMPM.FormattingEnabled = true;
            this.cmbBoxAMPM.Items.AddRange(new object[] {
            "PM",
            "AM"});
            this.cmbBoxAMPM.Location = new System.Drawing.Point(122, 156);
            this.cmbBoxAMPM.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBoxAMPM.Name = "cmbBoxAMPM";
            this.cmbBoxAMPM.Size = new System.Drawing.Size(119, 24);
            this.cmbBoxAMPM.TabIndex = 47;
            // 
            // btnAddTask
            // 
            this.btnAddTask.Location = new System.Drawing.Point(30, 199);
            this.btnAddTask.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(212, 29);
            this.btnAddTask.TabIndex = 48;
            this.btnAddTask.Text = "Add Tasks";
            this.btnAddTask.UseVisualStyleBackColor = true;
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // pnlMarkTaskComplete
            // 
            this.pnlMarkTaskComplete.AutoScroll = true;
            this.pnlMarkTaskComplete.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pnlMarkTaskComplete.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlMarkTaskComplete.Location = new System.Drawing.Point(761, 111);
            this.pnlMarkTaskComplete.Margin = new System.Windows.Forms.Padding(2);
            this.pnlMarkTaskComplete.Name = "pnlMarkTaskComplete";
            this.pnlMarkTaskComplete.Size = new System.Drawing.Size(342, 348);
            this.pnlMarkTaskComplete.TabIndex = 50;
            this.pnlMarkTaskComplete.WrapContents = false;
            this.pnlMarkTaskComplete.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMarkTaskComplete_Paint);
            // 
            // lblUncompletedPanel
            // 
            this.lblUncompletedPanel.AutoSize = true;
            this.lblUncompletedPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUncompletedPanel.Location = new System.Drawing.Point(420, 78);
            this.lblUncompletedPanel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUncompletedPanel.Name = "lblUncompletedPanel";
            this.lblUncompletedPanel.Size = new System.Drawing.Size(214, 20);
            this.lblUncompletedPanel.TabIndex = 52;
            this.lblUncompletedPanel.Text = "UNCOMPLETED TASKS";
            // 
            // lblMarkCompletePanel
            // 
            this.lblMarkCompletePanel.AutoSize = true;
            this.lblMarkCompletePanel.Location = new System.Drawing.Point(828, 81);
            this.lblMarkCompletePanel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMarkCompletePanel.Name = "lblMarkCompletePanel";
            this.lblMarkCompletePanel.Size = new System.Drawing.Size(208, 17);
            this.lblMarkCompletePanel.TabIndex = 53;
            this.lblMarkCompletePanel.Text = "VALIDATE TASK COMPLETION";
            // 
            // lblCompletedTasksPanel
            // 
            this.lblCompletedTasksPanel.AutoSize = true;
            this.lblCompletedTasksPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompletedTasksPanel.Location = new System.Drawing.Point(1201, 75);
            this.lblCompletedTasksPanel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCompletedTasksPanel.Name = "lblCompletedTasksPanel";
            this.lblCompletedTasksPanel.Size = new System.Drawing.Size(188, 20);
            this.lblCompletedTasksPanel.TabIndex = 55;
            this.lblCompletedTasksPanel.Text = "COMPLETED TASKS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Controls.Add(this.lblTotalTasksCounter);
            this.panel1.Controls.Add(this.lblTasksCompletedText);
            this.panel1.Controls.Add(this.lblTasksDoneCounter);
            this.panel1.Controls.Add(this.lblEmployeesCounterText);
            this.panel1.Controls.Add(this.lblEmployeesCounter);
            this.panel1.Controls.Add(this.lblAnimalsCounterText);
            this.panel1.Controls.Add(this.lblAnimalsCounter);
            this.panel1.Controls.Add(this.btnTasksCompleted);
            this.panel1.Controls.Add(this.btnTasksDoneByClass);
            this.panel1.Controls.Add(this.cmbBoxClassTasksDone);
            this.panel1.Location = new System.Drawing.Point(761, 480);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 292);
            this.panel1.TabIndex = 56;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lblTotalTasksCounter
            // 
            this.lblTotalTasksCounter.AutoSize = true;
            this.lblTotalTasksCounter.Location = new System.Drawing.Point(218, 29);
            this.lblTotalTasksCounter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalTasksCounter.Name = "lblTotalTasksCounter";
            this.lblTotalTasksCounter.Size = new System.Drawing.Size(24, 17);
            this.lblTotalTasksCounter.TabIndex = 34;
            this.lblTotalTasksCounter.Text = "/ 0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label4.Location = new System.Drawing.Point(80, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 17);
            this.label4.TabIndex = 57;
            this.label4.Text = "1. Add Employee";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label5.Location = new System.Drawing.Point(64, 16);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 17);
            this.label5.TabIndex = 58;
            this.label5.Text = "2. Add Animal";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label6.Location = new System.Drawing.Point(81, 30);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 59;
            this.label6.Text = "3. Add Task";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnAddEmployee);
            this.panel2.Controls.Add(this.lblnameEmployee);
            this.panel2.Controls.Add(this.txtBoxNameEmployee);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtboxLastName);
            this.panel2.Controls.Add(this.btnRemoveEmployee);
            this.panel2.Controls.Add(this.lblEmployeeAddedMessage);
            this.panel2.Location = new System.Drawing.Point(31, 98);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(271, 201);
            this.panel2.TabIndex = 60;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.btnAddAnimal);
            this.panel3.Controls.Add(this.lblName);
            this.panel3.Controls.Add(this.txtBoxAnimalName);
            this.panel3.Controls.Add(this.cmbBoxAnimalClasses);
            this.panel3.Controls.Add(this.btnRemoveAnimal);
            this.panel3.Controls.Add(this.lblAnimalAddedMessage);
            this.panel3.Location = new System.Drawing.Point(31, 318);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(271, 216);
            this.panel3.TabIndex = 61;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblAddTaskMessage);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.txtBoxPetName);
            this.panel4.Controls.Add(this.cmbBoxTasksOptions);
            this.panel4.Controls.Add(this.txtBoxHours);
            this.panel4.Controls.Add(this.txtBoxMinutes);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.cmbBoxAMPM);
            this.panel4.Controls.Add(this.btnAddTask);
            this.panel4.Location = new System.Drawing.Point(31, 548);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(271, 264);
            this.panel4.TabIndex = 61;
            // 
            // lblAddTaskMessage
            // 
            this.lblAddTaskMessage.AutoSize = true;
            this.lblAddTaskMessage.Location = new System.Drawing.Point(30, 235);
            this.lblAddTaskMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddTaskMessage.Name = "lblAddTaskMessage";
            this.lblAddTaskMessage.Size = new System.Drawing.Size(0, 17);
            this.lblAddTaskMessage.TabIndex = 60;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.label9.Location = new System.Drawing.Point(29, 161);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 17);
            this.label9.TabIndex = 34;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel5.Location = new System.Drawing.Point(12, 12);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(521, 10);
            this.panel5.TabIndex = 62;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel6.Location = new System.Drawing.Point(550, 12);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(442, 10);
            this.panel6.TabIndex = 63;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel7.Location = new System.Drawing.Point(1005, 12);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(515, 10);
            this.panel7.TabIndex = 63;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel8.Location = new System.Drawing.Point(1005, 829);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(515, 10);
            this.panel8.TabIndex = 66;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel9.Location = new System.Drawing.Point(550, 829);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(442, 10);
            this.panel9.TabIndex = 65;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel10.Location = new System.Drawing.Point(12, 829);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(521, 10);
            this.panel10.TabIndex = 64;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1532, 864);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblCompletedTasksPanel);
            this.Controls.Add(this.lblMarkCompletePanel);
            this.Controls.Add(this.lblUncompletedPanel);
            this.Controls.Add(this.pnlMarkTaskComplete);
            this.Controls.Add(this.CheckBoxPanel);
            this.Controls.Add(this.lblErrorFeed);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.pnlTasksDone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "PetTaskOrganizer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Label lblnameEmployee;
        private System.Windows.Forms.TextBox txtBoxNameEmployee;
        private System.Windows.Forms.TextBox txtboxLastName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxAnimalName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnAddAnimal;
        private System.Windows.Forms.ComboBox cmbBoxAnimalClasses;
        private System.Windows.Forms.Button btnRemoveEmployee;
        private System.Windows.Forms.Button btnRemoveAnimal;
        private System.Windows.Forms.Button btnTasksCompleted;
        private System.Windows.Forms.Label lblTasksCompletedText;
        private System.Windows.Forms.FlowLayoutPanel pnlTasksDone;
        private System.Windows.Forms.Button btnTasksDoneByClass;
        private System.Windows.Forms.ComboBox cmbBoxClassTasksDone;
        private System.Windows.Forms.Label lblTasksDoneCounter;
        private System.Windows.Forms.Label lblEmployeesCounter;
        private System.Windows.Forms.Label lblEmployeesCounterText;
        private System.Windows.Forms.Label lblAnimalsCounter;
        private System.Windows.Forms.Label lblAnimalsCounterText;
        private System.Windows.Forms.Label lblEmployeeAddedMessage;
        private System.Windows.Forms.Label lblAnimalAddedMessage;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblErrorFeed;
        private System.Windows.Forms.TextBox txtBoxPetName;
        private System.Windows.Forms.ComboBox cmbBoxTasksOptions;
        private System.Windows.Forms.TextBox txtBoxHours;
        private System.Windows.Forms.TextBox txtBoxMinutes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbBoxAMPM;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.FlowLayoutPanel pnlMarkTaskComplete;
        public System.Windows.Forms.FlowLayoutPanel CheckBoxPanel;
        private System.Windows.Forms.Label lblUncompletedPanel;
        private System.Windows.Forms.Label lblMarkCompletePanel;
        private System.Windows.Forms.Label lblCompletedTasksPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lblTotalTasksCounter;
        private System.Windows.Forms.Label lblAddTaskMessage;
    }
}