﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ConsolePetTasksOrganizer
{

    /*
   *Modules Covered:
   * 
   * Interfaces
   * Abstract clases
   * Inheritance
   * LAMDAS
   * Polymorphism
   * Override ToString
   * Generics(List<T>)
   */

    //INTERFACE <---------------------
    public interface IAnimal
    {
        string FoodType { get; set; }
        bool FeedDone { get; set; }
        bool CleanDone { get; set; }
        string Name { get; set; }

    }

    public class TasksTime
    {

        public string convertToTime(string hours, string minutes, string PmAm)
        {
            string time = hours + ":" + minutes + " " + PmAm;
            return time;
        }

        public bool IsSameTime(string time)
        {
            DateTime now = DateTime.Now;
            return now.ToString("t") == time;
        }

        public void AddTasksTimes(string a, string activity, string time)
        {
            Store.TasksTimes.Add(new Tuple<string, string, string>(a, activity, time));
        }

    }


    public class Dog : IAnimal
    {
        public string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        public string LeisureType = "Toys";
        public string _FoodType = "Dry food";
        public string Class = "Dog";
        public string FoodType { get { return this._FoodType; } set { FoodType = this._FoodType; } }
        public bool _FeedDone = false;
        public bool _CleanDone = false;
        public bool FeedDone { get => _FeedDone; set => _FeedDone = value; }
        public bool CleanDone { get => _CleanDone; set => _CleanDone = value; }
        public bool WalkDone = false;

        public int FeedMe = 7000;


        public override string ToString()
        {
            return Name;
        }
    }

    public class Cat : IAnimal
    {
        public string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        public string LeisureType = "Toys";
        public string _FoodType = "Dry food";
        public string Class = "Cat";
        public string FoodType { get => _FoodType; set => _FoodType = value; }
        public bool _FeedDone = false;
        public bool _CleanDone = false;
        public bool FeedDone { get => _FeedDone; set => _FeedDone = value; }
        public bool CleanDone { get => _CleanDone; set => _CleanDone = value; }

        public override string ToString()
        {
            return Name;
        }

    }

    //ABSTRACT CLASS <---------------------
    public abstract class Rodent
    {
        public string LeisureType = "Toys";
        public string Class = "Roedant";
    }


    public class Hamster : Rodent, IAnimal //Implementing both interface and abstract class
    {
        public string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        public string SubClass = "Hamster";
        public new string LeisureType = "Wheel";
        public string _FoodType = "pellet";
        public string FoodType { get => _FoodType; set => _FoodType = value; }
        public bool _FeedDone;
        public bool _CleanDone;
        public bool FeedDone { get => _FeedDone; set => _FeedDone = value; }
        public bool CleanDone { get => _CleanDone; set => _CleanDone = value; }


        public override string ToString()
        {
            return Name;
        }
    }

    public class GuineaPig : Rodent, IAnimal
    {
        public string SubClass = "Guinea Pig";
        public string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        public string _FoodType = "Veggies, fruits, pellet";
        public string FoodType { get => _FoodType; set => _FoodType = value; }
        public bool _FeedDone = false;
        public bool _CleanDone = false;
        public bool FeedDone { get => _FeedDone; set => _FeedDone = value; }
        public bool CleanDone { get => _CleanDone; set => _CleanDone = value; }

        public override string ToString()
        {
            return Name;
        }

    }

    public class Employee
    {
        public string Name { get; set; }
        public string LName { get; set; }

        public void FeedAnimal(IAnimal animal)
        {
            animal.FeedDone = true;
            AddTask(this, animal, "Fed");
        }

        public void CleanAnimal(IAnimal animal)
        {
            animal.CleanDone = true;
            AddTask(this, animal, "Cleaned");
        }

        public void WalkDog(Dog dog)
        {
            dog.WalkDone = true;
            AddTask(this, dog, "Walked");
        }

        public void AddTask(Employee e, IAnimal a, string activity)
        {
            Store.TasksLists.Add(new Tuple<Employee, IAnimal, string>(e, a, activity));
        }

        public override string ToString()
        {
            return Name + " " + LName;
        }

    }

    /// <summary>
    /// Contains list of animals in a store/shelter 
    /// Contains Employees for store management
    /// </summary>
    public class Store
    {
        public string Name;
        public List<IAnimal> Pets = new List<IAnimal>();
        public List<Employee> Employees = new List<Employee>();
        public static List<Tuple<string, string, string>> TasksTimes = new List<Tuple<string, string, string>>();
        public static List<Tuple<Employee, IAnimal, string>> TasksLists = new List<Tuple<Employee, IAnimal, string>>();
        public Store(string name)
        {
            Name = name;
        }

        public void AddEmployee(Employee employee)
        {
            Employees.Add(employee);
            Console.WriteLine("{0} Added sucessfully ", employee);
        }

        public void RemoveEmployee(Employee employee)
        {
            Employees.Remove(employee);
            Console.WriteLine("{0} Removed sucessfully", employee);
        }

        public void AddAnimal(IAnimal animal)
        {
            Pets.Add(animal);
        }

        public void RemoveAnimal(IAnimal animal)
        {
            Pets.Remove(animal);
            Console.WriteLine("Removed Sucessfully");
        }

        public void TasksCompleted()
        {
            Console.WriteLine("Tasks completed today");
            TasksLists.ForEach((element) => Console.WriteLine(element));
        }

        public void TasksCompleted(string ThisClass)
        {

            Console.WriteLine("Tasks completed for {0}s", ThisClass);

            foreach (var element in TasksLists)
            {

                var text = element.Item2.GetType().ToString();
                text = text.Substring(text.IndexOf('.') + 1);
                if (text.Equals(ThisClass))
                    Console.WriteLine(element);
            }
        }
    }

    public partial class Program
    {
        static void Main(string[] args)
        {
            //Connect Form to class
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
