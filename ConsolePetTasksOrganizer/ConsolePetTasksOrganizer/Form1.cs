﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsolePetTasksOrganizer
{
    /*
    *Modules Covered:
    *Events
    *Exception Handling
    *LINQ/LAMDAS
    *Delegate
    *Predicate Delegates
    */

    public partial class Form1 : Form
    {
        Store PetStore = new Store("PetStore");
        TasksTime timetasks = new TasksTime();

        public Form1()
        {
            InitializeComponent();
        }

        //EVENT<----------------------------------
        //timer is the event to send notifications
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();

        // this is the SUBSCRIBER<----------------------------------
        private void TimerEventProccesor(Object myObject, EventArgs myEventArgs)
        {
            myTimer.Stop();
            TasksTime tasks = new TasksTime();
            if (Store.TasksTimes == null) return;
            else
            {
                //Look tasks that mathes current time
                Store.TasksTimes.ForEach((element) =>
                {

                    //This is where it compares
                    if (!tasks.IsSameTime(element.Item3)) return;
                    else
                    {
                        TaskUserControl notificationTextbox = new TaskUserControl();
                        notificationTextbox.checkBox1.Click += FindCheckedLabel;//When the notication is clicked 
                        var text = element.Item2.ToString() + " " + element.Item1.ToString() + " at " + element.Item3.ToString();

                        notificationTextbox.checkBox1.Text = text;
                        CheckBoxPanel.Controls.Add(notificationTextbox);

                    }

                });
            }


            myTimer.Start();
        }

        //Function for when a task is marked complete
        private void FindCheckedLabel(object sender, EventArgs e)
        {
            var messageTimer = new Timer();
            messageTimer.Interval = 3000;

            foreach (TaskUserControl c in CheckBoxPanel.Controls)
            {
                if (c.checkBox1.Checked && c.lblCheckStatus.Text.Equals("formNotDisplayed"))
                {
                    var textArr = c.checkBox1.Text.Split(' ');
                    var action = textArr[0];
                    var petName = textArr[1];

                    MarkTaskCompleteUserControl taskForm = new MarkTaskCompleteUserControl();
                    taskForm.lblActivityName.Text = action + " " + petName;
                    taskForm.btnCompleteActivity.Text = action + " Done";
                    taskForm.btnCompleteActivity.Click += (sen, ev) =>
                        {
                            //HANDLE EXCEPTION <----------------------------------
                            try
                            {
                                //LINQ LAMDAS <----------------------------------
                                var animal = PetStore.Pets.FirstOrDefault(n => n.Name == petName); //< ----------------------------------
                                var empl = PetStore.Employees.FirstOrDefault(n => n.Name == taskForm.txtBoxEmployeeName.Text); //< ----------------------------------
                                if (action.Equals("Clean"))
                                {
                                    empl.CleanAnimal(animal);

                                }
                                else if (action.Equals("Feed"))
                                {
                                    empl.FeedAnimal(animal);
                                }
                                else if (action.Equals("Walk"))
                                {
                                    var checkType = animal.GetType().ToString();
                                    checkType = checkType.Substring(checkType.IndexOf('.') + 1);
                                    if (checkType.Equals("Dog"))
                                    {
                                        Dog d = new Dog();
                                        d = (Dog)animal;
                                        empl.WalkDog(d);
                                    }

                                }
                                lblTasksDoneCounter.Text = Store.TasksLists.Count().ToString();
                                taskForm.lblError.ForeColor = Color.Green;
                                taskForm.lblError.Text = " '" + action + " " + petName + "' " + " Completed";
                                taskForm.lblError.Visible = true;
                                CheckBoxPanel.Controls.Remove(c);
                                messageTimer.Tick += (s, en) =>
                                {
                                    pnlMarkTaskComplete.Controls.Remove(taskForm);
                                    messageTimer.Stop();

                                };
                                messageTimer.Start();

                                return;
                            }
                            catch (Exception)
                            {
                                taskForm.lblError.ForeColor = Color.Red;
                                taskForm.lblError.Text = "Pet/Employee not found";
                                taskForm.lblError.Visible = true;

                                return;
                            }
                        };
                    pnlMarkTaskComplete.Controls.Add(taskForm);
                    c.lblCheckStatus.Text = "formDisplayed";
                }
            }
        }

        //It gets activated when we open the app
        private void Form1_Load(object sender, EventArgs e)
        {
            // Subscription  (check tasks and time ) to the event (Timer) happens here 
            myTimer.Tick += new EventHandler(TimerEventProccesor);
            myTimer.Interval = 60000;//1 min
            myTimer.Start();
        }

        //button that adds tasks with specific time to 'TasksTimes' List of the store
        private void btnAddTask_Click(object sender, EventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            // EXCEPTION HANDLING < ----------------------------------
            try
            {
                var hour = txtBoxHours.Text;
                var minutes = txtBoxMinutes.Text;
                var ampm = cmbBoxAMPM.Text;
                var task = cmbBoxTasksOptions.Text;
                var name = txtBoxPetName.Text;

                // PREDICATE DELEGATE // LINQ LAMDAS <----------------------------------
                Predicate<string> exist = delegate (string s) { return PetStore.Pets.Any(n => n.Name == name); };
                bool result = exist(name);

                if (result == false)
                {
                    throw new ArgumentException();
                }

                if (hour == "" || minutes == "" || ampm == "" || task == "")
                {
                    throw new ArgumentNullException();
                }

                else
                {
                    // Convert to the right format to be able to compare
                    var Time = timetasks.convertToTime(hour, minutes, ampm);

                    timetasks.AddTasksTimes(name, task, Time);

                    Store.TasksTimes.ForEach((element) =>
                    {
                    });
                    lblTotalTasksCounter.Text = "/ " + Store.TasksTimes.Count();

                    lblAddTaskMessage.ForeColor = Color.Green;
                    lblAddTaskMessage.Text = " Task added successfully";
                    lblAddTaskMessage.Show();

                    //Label that dissapears after 3 seconds
                    t.Tick += (s, en) =>
                    {
                        lblAddTaskMessage.Hide();
                        t.Stop();
                    };
                    t.Start();
                }

            }
            catch (ArgumentNullException)
            {
                lblAddTaskMessage.ForeColor = Color.Red;
                lblAddTaskMessage.Text = "You must complete all fields";
                lblAddTaskMessage.Show();

                t.Tick += (s, en) =>
                {
                    lblEmployeeAddedMessage.Hide();
                    t.Stop();
                };

                t.Start();
                return;
            }
            catch (ArgumentException)
            {
                lblAddTaskMessage.ForeColor = Color.Red;
                lblAddTaskMessage.Text = "Pet Name not found";
                lblAddTaskMessage.Show();

                t.Tick += (s, en) =>
                {
                    lblEmployeeAddedMessage.Hide();
                    t.Stop();
                };

                t.Start();
                return;
            }

        }

        //Add/hire new Employee to store
        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            //EXCEPTION HANDLING < ----------------------------------
            try
            {
                var name = txtBoxNameEmployee.Text;
                var lastname = txtboxLastName.Text;

                if (name == "" || lastname == "")
                {
                    throw new InvalidExpressionException();
                }
                else
                {
                    PetStore.AddEmployee(new Employee { Name = name, LName = lastname });
                    lblEmployeesCounter.Text = PetStore.Employees.Count().ToString();
                    lblEmployeeAddedMessage.ForeColor = Color.Green;
                    lblEmployeeAddedMessage.Text = name + " " + lastname + " " + "added successfully";
                    lblEmployeeAddedMessage.Show();

                    //Label that dissapears after 3 seconds
                    t.Tick += (s, en) =>
                    {
                        lblEmployeeAddedMessage.Hide();
                        t.Stop();
                    };

                    t.Start();
                }
            }
            catch (InvalidExpressionException)
            {
                lblEmployeeAddedMessage.ForeColor = Color.Red;
                lblEmployeeAddedMessage.Text = "You must complete all fields";
                lblEmployeeAddedMessage.Show();

                t.Tick += (s, en) =>
                {
                    lblEmployeeAddedMessage.Hide();
                    t.Stop();
                };

                t.Start();
                return;
            }
        }

        //Adds a new animal to the store list
        private void btnAddAnimal_Click(object sender, EventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;

            //PREDICATE DELEGATE <----------------------------------
            Predicate<string> exist = delegate (string s) { return PetStore.Pets.Exists(x => x.Name == s); };

            //EXCEPTION HANDLING <----------------------------------
            try
            {
                var name = txtBoxAnimalName.Text;

                //Checking if name already exist using predicates
                bool result = exist(name);

                if (name == "" || cmbBoxAnimalClasses.Text == "Type")
                {
                    throw new ArgumentNullException();
                }
                if (result == true)
                {
                    throw new InvalidExpressionException();
                }
                else
                {
                    if (cmbBoxAnimalClasses.Text == "Dog")
                    {
                        Dog d = new Dog() { Name = name };
                        PetStore.AddAnimal(d);
                    }
                    else if (cmbBoxAnimalClasses.Text == "Cat")
                    {
                        PetStore.AddAnimal(new Cat { Name = name });

                    }
                    else if (cmbBoxAnimalClasses.Text == "Hamster")
                    {
                        PetStore.AddAnimal(new Hamster { Name = name });
                    }
                    else if (cmbBoxAnimalClasses.Text == "Guinea Pig")
                    {
                        PetStore.AddAnimal(new GuineaPig { Name = name });
                    }

                    lblAnimalsCounter.Text = PetStore.Pets.Count().ToString();
                    lblAnimalAddedMessage.ForeColor = Color.Green;
                    lblAnimalAddedMessage.Text = name + " " + "added successfully";
                    lblAnimalAddedMessage.Show();

                    t.Tick += (s, en) =>
                    {
                        lblAnimalAddedMessage.Hide();
                        t.Stop();
                    };
                    t.Start();
                }
            }
            catch (ArgumentNullException)
            {
                lblAnimalAddedMessage.Text = "You must complete all fields";
                lblAnimalAddedMessage.ForeColor = Color.Red;
                lblAnimalAddedMessage.Show();
                t.Tick += (s, en) =>
                {
                    lblAnimalAddedMessage.Hide();
                    t.Stop();
                };
                t.Start();
            }
            catch (InvalidExpressionException)
            {
                lblAnimalAddedMessage.Text = "Pet Name already exist";
                lblAnimalAddedMessage.ForeColor = Color.Red;
                lblAnimalAddedMessage.Show();
                t.Tick += (s, en) =>
                {
                    lblAnimalAddedMessage.Hide();
                    t.Stop();
                };
                t.Start();
            }
        }

        //Removes(fire) employee from the store lists
        private void btnRemoveEmployee_Click(object sender, EventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;
            //PREDICATE DELEGATE
            Predicate<string> NameExist = delegate (string s) { return PetStore.Employees.Exists(x => x.Name == s); };
            Predicate<string> LastExist = delegate (string s) { return PetStore.Employees.Exists(x => x.LName == s); };

            try
            {
                var name = txtBoxNameEmployee.Text;
                var lastname = txtboxLastName.Text;
                bool existsName = NameExist(name);
                bool existLast = LastExist(lastname);

                if (name == "" || lastname == "")
                {
                    throw new InvalidExpressionException();
                }
                if (existsName == false)
                {
                    throw new NullReferenceException();
                }
                if (existLast == false)
                {
                    throw new NullReferenceException();
                }

                else
                {
                    var employee = PetStore.Employees.FirstOrDefault(n => n.Name == name);
                    var employeeLast = PetStore.Employees.FirstOrDefault(n => n.LName == name);


                    PetStore.RemoveEmployee(employee);
                    lblEmployeesCounter.Text = PetStore.Employees.Count().ToString();
                    lblEmployeeAddedMessage.Text = employee + " " + "removed successfully";
                    lblEmployeeAddedMessage.ForeColor = Color.Green;
                    lblEmployeeAddedMessage.Show();

                    //Label that dissapears after 3 seconds
                    t.Tick += (s, en) =>
                    {
                        lblEmployeeAddedMessage.Hide();
                        t.Stop();
                    };
                    t.Start();


                }

            }
            catch (InvalidExpressionException)
            {
                lblEmployeeAddedMessage.Text = "You must complete all fields";
                lblEmployeeAddedMessage.ForeColor = Color.Red;
                lblEmployeeAddedMessage.Show();

                t.Tick += (s, en) =>
                {
                    lblEmployeeAddedMessage.Hide();
                    t.Stop();
                };
                t.Start();
                return;
            }
            catch (NullReferenceException)
            {
                lblEmployeeAddedMessage.Text = "Employee not found";
                lblEmployeeAddedMessage.ForeColor = Color.Red;
                lblEmployeeAddedMessage.Show();

                t.Tick += (s, en) =>
                {
                    lblEmployeeAddedMessage.Hide();
                    t.Stop();
                };
                t.Start();
                return;
            }
        }

        //Removes Animal from the Store Lists
        private void btnRemoveAnimal_Click(object sender, EventArgs e)
        {
            var t = new Timer();
            t.Interval = 3000;

            Predicate<string> exist = delegate (string s) { return PetStore.Pets.Exists(x => x.Name == s); };

            try
            {
                var name = txtBoxAnimalName.Text;
                bool exists = exist(name);
                var animal = PetStore.Pets.FirstOrDefault(n => n.Name == name);

                if (animal == null)
                {
                    throw new Exception();
                }

                else if (!animal.GetType().ToString().Substring(animal.GetType().ToString().IndexOf('.') + 1).Equals(cmbBoxAnimalClasses.Text))
                {
                    throw new ArgumentNullException();
                }
                else if (name == "" || cmbBoxAnimalClasses.Text == "Type")
                {
                    throw new InvalidExpressionException();
                }
                else if (exists == false)
                {
                    throw new ArgumentNullException();
                }

                else
                {
                    PetStore.RemoveAnimal(animal);
                    lblAnimalsCounter.Text = PetStore.Pets.Count().ToString();
                    lblAnimalAddedMessage.Text = animal + " " + "removed successfully";
                    lblAnimalAddedMessage.ForeColor = Color.Green;
                    lblAnimalAddedMessage.Show();

                }
            }
            catch (InvalidExpressionException)
            {
                lblAnimalAddedMessage.Text = "You must complete all fields";
                lblAnimalAddedMessage.ForeColor = Color.Red;
                lblAnimalAddedMessage.Show();
                t.Tick += (s, en) =>
                {
                    lblAnimalAddedMessage.Hide();
                    t.Stop();
                };
                t.Start();
            }
            catch (Exception)
            {
                lblAnimalAddedMessage.Text = "Pet not found ";
                lblAnimalAddedMessage.ForeColor = Color.Red;
                lblAnimalAddedMessage.Show();
                t.Tick += (s, en) =>
                {
                    lblAnimalAddedMessage.Hide();
                    t.Stop();
                };
                t.Start();
            }
        }

        //Displays tasks that are completed
        private void btnTasksCompleted_Click(object sender, EventArgs e)
        {
            pnlTasksDone.Controls.Clear();
            Store.TasksLists.ForEach((element) =>
            {
                Label lbl = new Label();
                lbl.AutoSize = true;
                lbl.Margin = new Padding(32, 30, 10, 0);
                lbl.Text = element.Item2.ToString() + " " + element.Item3.ToString() + " by " + element.Item1.ToString();
                pnlTasksDone.Controls.Add(lbl);
            });


        }

        //Displays tasks that are completed by the type of animal the user chooses
        private void btnTasksDoneByClass_Click(object sender, EventArgs e)
        {
            pnlTasksDone.Controls.Clear();
            var className = cmbBoxClassTasksDone.Text;

            foreach (var element in Store.TasksLists)
            {
                var text = element.Item2.GetType().ToString();
                text = text.Substring(text.IndexOf('.') + 1);
                if (text.Equals(className))
                {
                    Label lbl = new Label();
                    lbl.AutoSize = true;
                    lbl.Margin = new Padding(32, 10, 10, 10);
                    lbl.Text = element.Item2.ToString() + " " + element.Item3.ToString() + " by " + element.Item1.ToString();
                    pnlTasksDone.Controls.Add(lbl);
                }
            }
        }

        private void pnlMarkTaskComplete_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblnameEmployee_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
