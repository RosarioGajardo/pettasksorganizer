﻿namespace ConsolePetTasksOrganizer
{
    partial class MarkTaskCompleteUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblActivityName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxEmployeeName = new System.Windows.Forms.TextBox();
            this.btnCompleteActivity = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblActivityName
            // 
            this.lblActivityName.AutoSize = true;
            this.lblActivityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActivityName.Location = new System.Drawing.Point(45, 60);
            this.lblActivityName.Name = "lblActivityName";
            this.lblActivityName.Size = new System.Drawing.Size(118, 17);
            this.lblActivityName.TabIndex = 1;
            this.lblActivityName.Text = "lblActivityName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter Employee\'s Name: ";
            // 
            // txtBoxEmployeeName
            // 
            this.txtBoxEmployeeName.Location = new System.Drawing.Point(48, 117);
            this.txtBoxEmployeeName.Name = "txtBoxEmployeeName";
            this.txtBoxEmployeeName.Size = new System.Drawing.Size(215, 22);
            this.txtBoxEmployeeName.TabIndex = 3;
            // 
            // btnCompleteActivity
            // 
            this.btnCompleteActivity.Location = new System.Drawing.Point(48, 159);
            this.btnCompleteActivity.Name = "btnCompleteActivity";
            this.btnCompleteActivity.Size = new System.Drawing.Size(215, 47);
            this.btnCompleteActivity.TabIndex = 4;
            this.btnCompleteActivity.Text = "Done";
            this.btnCompleteActivity.UseVisualStyleBackColor = true;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(45, 20);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(46, 17);
            this.lblError.TabIndex = 5;
            this.lblError.Text = "label2";
            this.lblError.Visible = false;
            // 
            // MarkTaskCompleteUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnCompleteActivity);
            this.Controls.Add(this.txtBoxEmployeeName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblActivityName);
            this.Name = "MarkTaskCompleteUserControl";
            this.Size = new System.Drawing.Size(312, 245);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label lblError;
        public System.Windows.Forms.Label lblActivityName;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtBoxEmployeeName;
        public System.Windows.Forms.Button btnCompleteActivity;
    }
}
